#!/usr/bin/env python3

import logging
import re
from argparse import ArgumentParser, Namespace
from datetime import datetime, timedelta
from logging import Logger
from pathlib import Path
from pprint import pformat
from re import Pattern
from typing import List, Optional, Set

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml
from matplotlib.dates import DateFormatter
from matplotlib.pyplot import Axes, Figure
from matplotlib.ticker import FixedLocator, MaxNLocator
from pandas import DataFrame

log: Logger = logging.getLogger("main")
logging.basicConfig()

week_regex: Pattern = re.compile("(?<=W)[0-9]+")
assignment_regex: Pattern = re.compile("W[0-9]+\s*.*")
grade_ordering: List[str] = ["A", "A-", "B+", "B", "B-", "C+", "C", "C-", "F"]

plt.style.use("bmh")


def plot_grade_distribution(df: DataFrame, title: str, filename: Path):
    fig, ax = plt.subplots()
    grade_counts: DataFrame = df["Current Grade"].value_counts()
    for grade in grade_ordering:
        if grade not in grade_counts.index:
            grade_counts[grade] = 0
    grade_counts = grade_counts[grade_ordering]
    ax.bar(grade_counts.index, grade_counts.values)
    ax.set_title(title)
    ax.set_ylabel("Count")
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    fig.savefig(
        filename,
        dpi=300,
        bbox_inches="tight",
    )
    plt.close("all")


def identify_assignment_group(week: str, title: str) -> str:
    if "Solo" in title:
        return "solo"

    if "Team" in title:
        if int(week) < 9:
            return "team"
        else:
            return "final_project"

    if "Ponder" in title:
        return "ponder"

    return "other"

def get_sorted_grade_files(args: Namespace) -> List[Path]:
    log.info("Loading grade exports from %s", args.grades_dir)

    paths: List[Path] = []

    for grades_file in sorted(args.grades_dir.iterdir()):
        try:
            datetime.strptime(
                str(grades_file),
                f"{str(args.grades_dir)}/%Y-%m-%dT%H%M_Grades-CSE_210.csv",
            )
            paths.append(grades_file)
        except ValueError as err:
            log.debug("Got ValueError extracting time from filename:\n%s", err)
            continue

    return paths

def get_sorted_student_files(args: Namespace) -> List[Path]:
    log.info("Loading student exports from %s", args.grades_dir)

    paths: List[Path] = []

    for students_file in sorted(args.grades_dir.iterdir()):
        try:
            datetime.strptime(
                str(students_file),
                f"{str(args.grades_dir)}/%Y-%m-%dT%H:%M:%S%z_students.csv",
            )
            paths.append(students_file)
        except ValueError as err:
            log.debug("Got ValueError extracting time from filename:\n%s", err)
            continue

    return paths


def do_grade_plots(args: Namespace):
    grade_files: List[Path] = get_sorted_grade_files(args)

    mean_scores: List[Float] = []
    timestamps: List[datetime] = [
        datetime.strptime(
            str(grades_file),
            f"{str(args.grades_dir)}/%Y-%m-%dT%H%M_Grades-CSE_210.csv",
        )
        for grades_file in grade_files
    ]
    dfs: List[DataFrame] = []

    for grades_file in grade_files:
        date: datetime = datetime.strptime(
            str(grades_file),
            f"{str(args.grades_dir)}/%Y-%m-%dT%H%M_Grades-CSE_210.csv",
        )

        grades_df: DataFrame = pd.read_csv(grades_file)
        log.debug("Loaded grade export %s", str(grades_file))
        log.debug("%s", grades_df.head())

        grades_df = grades_df[grades_df["ID"].notna()]
        grades_df["Current Score"] = pd.to_numeric(grades_df["Current Score"])

        dfs.append(grades_df)

        mean_score: float = grades_df["Current Score"].mean()
        mean_scores.append(mean_score)
        print(f"Average score {date.strftime('%Y-%m-%d')}: {mean_score:.4}")

        # Current grade distribution
        log.debug("Plotting grade distribution")
        fig, ax = plt.subplots()
        grade_counts: DataFrame = grades_df["Current Grade"].value_counts()
        for grade in grade_ordering:
            if grade not in grade_counts.index:
                grade_counts[grade] = 0
        grade_counts = grade_counts[grade_ordering]
        ax.bar(grade_counts.index, grade_counts.values)
        ax.set_title(f"Grades Distribution as of {date.strftime('%Y-%m-%d')}")
        ax.set_ylabel("Count")
        ax.yaxis.set_major_locator(MaxNLocator(integer=True))
        fig.savefig(
            args.output / f"grade_distribution_{date.isoformat()}.png",
            dpi=300,
            bbox_inches="tight",
        )
        plt.close("all")

    log.info("Loaded %d grade exports", len(timestamps))

    mean_scores: np.ndarray = np.array(mean_scores)

    fig, ax = plt.subplots()
    ax.plot(timestamps, mean_scores)
    ax.set_title("Mean Class Score Trend")
    ax.set_ylabel("Mean grade")
    ax.set_xlabel("Date")
    fig.autofmt_xdate()
    fig.savefig(args.output / "grade_trend_mean.png", dpi=300, bbox_inches="tight")
    plt.close("all")

    students: Set[str] = set()
    for df in dfs:
        students |= set(df["Student"].dropna())
    student_grades = {s: [] for s in students}
    for df in dfs:
        tdf: DataFrame = df.set_index("Student")
        for student in students:
            try:
                student_grades[student].append(tdf.loc[student])
            except KeyError:
                student_grades[student].append(
                    student_grades[student][-1]
                    if len(student_grades[student]) > 0
                    else {"Current Score": 0}
                )
    fig, ax = plt.subplots()
    for student, grades in student_grades.items():
        ax.plot(timestamps, [g["Current Score"] for g in grades])
    ax.plot(timestamps, mean_scores, dashes=[2, 2], color="black", label="Mean score")
    ax.set_title("Student Scores")
    ax.set_xlabel("Date")
    ax.set_ylabel("Score")
    ax.legend()
    ax.grid(True)
    fig.autofmt_xdate()
    fig.savefig(args.output / "grade_trend_all.png", dpi=300, bbox_inches="tight")
    plt.close("all")


def do_participation_plots(args: Namespace):
    grade_files: List[Path] = get_sorted_grade_files(args)
    student_files: List[Path] = get_sorted_student_files(args)
    timestamps: List[datetime] = [
        datetime.strptime(
            str(students_file),
            f"{str(args.grades_dir)}/%Y-%m-%dT%H:%M:%S%z_students.csv",
        )
        for students_file in student_files
    ]

    for students_file in student_files:
        df: DataFrame = pd.read_csv(students_file)

        date: timestamp = datetime.strptime(
            str(students_file),
            f"{str(args.grades_dir)}/%Y-%m-%dT%H:%M:%S%z_students.csv",
        )

        df = df.drop(np.where(df["Last participation time"] == "-")[0])
        df["Last participation time"] = pd.to_datetime(df["Last participation time"])

        fig, ax = plt.subplots()
        ax.scatter(df["Last participation time"], df["Overall course grade"])
        ax.set_title("Participation vs. overall grade")
        ax.set_ylabel("Overall course grade")
        ax.set_xlabel("Last participation time")
        date_formatter: DateFormatter = DateFormatter("%y-%m-%d")
        ax.xaxis.set_major_formatter(date_formatter)
        fig.autofmt_xdate()
        fig.savefig(args.output / f"participation_score_{date.isoformat()}.png")
        plt.close("all")

    grades_df: DataFrame = pd.read_csv(grade_files[-1])
    grades_df = grades_df[grades_df["ID"].notna()].copy()
    grades_df["Current Score"] = pd.to_numeric(grades_df["Current Score"])
    grades_df = grades_df[~grades_df["SIS User ID"].isna()]
    grades_df["SIS User ID"] = pd.to_numeric(grades_df["SIS User ID"]).astype(int)
    grades_export_mean: float = grades_df['Current Score'].mean()
    print(f"Current mean score from grades export: {grades_export_mean}")

    students_df: DataFrame = pd.read_csv(student_files[-1])
    students_df = students_df.drop(np.where(students_df["Last participation time"] == "-")[0])
    students_df["Last participation time"] = pd.to_datetime(students_df["Last participation time"])
    students_export_mean: float = students_df["Overall course grade"].mean()
    print(f"Current mean score from students export: {students_export_mean}")
    students_df = students_df[students_df["Last participation time"] > (datetime.now() - timedelta(weeks=2))]

    filtered_grades_df: DataFrame = grades_df[grades_df["SIS User ID"].isin(students_df["SIS Id"])].copy()
    filtered_grades_export_mean: float = filtered_grades_df["Current Score"].mean()
    print(f"Current filtered mean score from grades export: {filtered_grades_export_mean}")

    date: timestamp = datetime.strptime(
        str(student_files[-1]),
        f"{str(args.grades_dir)}/%Y-%m-%dT%H:%M:%S%z_students.csv",
    )

    plot_grade_distribution(
        filtered_grades_df,
        f"Filtered Grades Distribution as of {date.strftime('%Y-%m-%d')}",
        args.output / f"filtered_grade_distribution_{date.isoformat()}.png",
    )


def parse_args(arglist: Optional[List[str]] = None) -> Namespace:
    argp: ArgumentParser = ArgumentParser(prog="grade-trends")
    argp.add_argument("grades_dir", help="path to grades files", type=Path)
    argp.add_argument(
        "-o",
        "--output",
        help="output directory",
        type=Path,
        default=Path(".") / "output",
    )
    argp.add_argument(
        "-d", "--debug", help="run in debug mode", action="store_true", default=False
    )

    return argp.parse_args()


def main(args):
    if args.debug:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)

    log.debug("Running with args:\n%s", pformat(vars(args)))

    if not args.output.exists():
        log.debug("Creating output directory")
        args.output.mkdir()

    do_grade_plots(args)
    do_participation_plots(args)


if __name__ == "__main__":
    _args = parse_args()
    main(_args)

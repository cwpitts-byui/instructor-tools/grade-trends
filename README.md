# Grade Trends

Compute grade trends and generate plots.

## Usage
You will need exports from Canvas' gradebook and the student overview
from the "new analytics" page. Both file exports should be named with
a timestamp (the gradebook export automatically has this). Place the
CSV files in a data directory and call the script with the path to
that directory as the first argument:

```
python3 main.py data
```

Output will be written to `STDOUT`, and plots will be placed in the
`output` folder (created if not present).
